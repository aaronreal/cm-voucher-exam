module.exports = {
  content: ["./assets/**/*.{scss}", "./public/index.html"],
  theme: {
    extend: {
      colors: {
        "main-white": "#e9e8e4",
        "main-light-green": "#8d9882",
        "main-green": "#213e1d",
        "font-main-green": "#223e1e",
      },
      fontFamily: {
        roboto: "Roboto",
        camdenslab: "CamdenSlab",
        camdensans: "CamdenSans",
      },
      width: {
        "pc-55": "55%",
        "pc-45": "45%",
      },
      lineHeight: {
        full: "100%",
      },
      letterSpacing: {
        dish: "0.2em",
      },
    },
  },
  plugins: [],
};
