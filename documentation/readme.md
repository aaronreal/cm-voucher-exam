# Camden Market Voucher Campaign HTML Test 

In this example, you have been given the design [camden-market-voucher-campaign-layouts.ai](designs/camden-market-voucher-campaign-layouts.ai) in the `/designs` folder by our marketing team.
The brief is to take the illustrator file and build a responsive layout based on the 2 artboards provided in the document

## The layout should meet the following criteria:

1. On desktop, the layout should be center aligned in the viewport
2. It should resize gracefully on smaller desktop screens
3. It should switch to the mobile artwork view on tablet and mobile viewports
4. It should use Bootstrap or the Tailwind framework
5. The exporting of assets and their format is up to you
6. Optionally, it will be built using sass and commited to git with instructions on how to compile and view the template

## Things not required for the test

You do *not* need to:

1. Build form validation as part of the test
2. Connect the form to any real endpoint