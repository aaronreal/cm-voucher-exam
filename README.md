# Voucher Campaign Test

### Live Demo
[CLICK HERE](https://aaronreal.gitlab.io/cm-voucher-exam/)

### Libraries Used

- Laravel Mix Standalone (Webpack compilation)
- Tailwind v3

### Prerequisite

- NodeJS 10+
- Yarn

### Usage

- Run `yarn install`
- Run `yarn mix watch` to enter watch compilation mode and access local development through `localhost:3000`
