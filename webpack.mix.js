const mix = require("laravel-mix");

require("mix-tailwindcss");
require("laravel-mix-serve");

mix
  .setPublicPath("public")
  .sass("assets/sass/main.scss", "public/dist")
  .tailwind()
  .serve("yarn serve");
